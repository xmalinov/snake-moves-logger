const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const History = require('../models/history.js')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(cors())

// DB Setup
const mongoose = require('mongoose')

const DATABASE_URL = process.env.DATABASE_URL || 'http://localhost'
mongoose.connect(`mongodb://${DATABASE_URL}/posts`)

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => {
  console.log('Connection Succeeded')
})

// SERVER Setup
app.get('/history', (req, res) => {
  History.find({}, 'history', (error, history) => {
    if (error) { console.error(error) }
    res.send({
      history: history
    })
  }).sort({_id: 1})
})

// History Endpoints
app.post('/history', (req, res) => {
  const db = req.db
  const history = req.body.history
  const newHistory = new History({
    history: history
  })

  newHistory.save((error) => {
    if (error) {
      console.log(error)
    }
    res.send({
      success: true,
      message: 'History saved successfully!'
    })
  })
})

// Fetch single history
app.get('/history/:id', (req, res) => {
  const db = req.db
  History.findById(req.params.id, 'history', (error, history) => {
    if (error) { console.error(error) }
    res.send(history)
  })
})

app.listen(process.env.PORT || 8081)
