export default class Grid {
  constructor (gridParams, ctx) {
    this.width = gridParams.width
    this.height = gridParams.height
    this.blockarea = gridParams.blockarea
    this.margin = gridParams.margin
    this.blocksize = gridParams.blockarea - gridParams.margin

    this.ctx = ctx

    this.generateDataSet()
  }
  generateDataSet () {
    this.dataset = []
    for (let x = 0; x < this.width; x++) {
      this.dataset[x] = [this.height]
      for (let y = 0; y < this.height; y++) {
        this.setPoint({ x, y }, 'empty')
      }
    }
  }
  get currentDataset () {
    return this.dataset
  }
  set currentDataset (dataset) {
    this.dataset = dataset
  }
  setPoint (point, state) {
    this.dataset[point.x][point.y] = state
  }
  getPoint (point) {
    return this.dataset[point.x][point.y]
  }
  getFillStyle (state) {
    switch (state) {
      case 'snake':
        return 'rgba(22,135,167,0.8)'
      case 'empty':
        return 'rgba(234,234,234,0.4)'
      case 'apple':
        return 'rgba(221,10,53,0.8)'
      default:
        return 'rgba(234,234,234,0.4)'
    }
  }
  render () {
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        const point = { x, y }
        const state = this.getPoint(point)
        this.ctx.fillStyle = this.getFillStyle(state)
        this.ctx.clearRect(point.x * this.blockarea, point.y * this.blockarea, this.blocksize, this.blocksize)
        this.ctx.fillRect(point.x * this.blockarea, point.y * this.blockarea, this.blocksize, this.blocksize)
      }
    }
  }
}
