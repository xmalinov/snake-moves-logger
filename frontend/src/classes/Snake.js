export default class Snake {
  constructor (positions, direction) {
    this.parts = [...positions]
    this.direction = direction
    this.newDirection = null
    this.prevTail = null
  }

  get currentParts () {
    return this.parts
  }

  update () {
    const updates = [...this.parts]

    if (this.newDirection) {
      this.direction = this.newDirection
      this.newDirection = null
    }

    const next = this.updatePosition()
    updates.unshift(next)
    this.prevTail = updates.pop()

    this.parts = [...updates]
  }

  updatePosition () {
    const next = Object.assign({}, this.parts[0])
    switch (this.direction) {
      case 'up':
        next.y--
        break
      case 'down':
        next.y++
        break
      case 'right':
        next.x++
        break
      case 'left':
        next.x--
        break
    }
    return next
  }

  changeDirection (direction) {
    if (this.newDirection) return
    switch (direction) {
      case 'left':
        this.newDirection = this.direction !== 'right' ? 'left' : 'right'
        break
      case 'up':
        this.newDirection = this.direction !== 'down' ? 'up' : 'down'
        break
      case 'right':
        this.newDirection = this.direction !== 'left' ? 'right' : 'left'
        break
      case 'down':
        this.newDirection = this.direction !== 'up' ? 'down' : 'up'
        break
    }
  }

  get head () {
    return Object.assign({}, this.parts[0])
  }

  grow () {
    // this.parts.push(Object.assign({}, this.parts[this.parts.length - 1]))
    this.parts.push(Object.assign({}, this.prevTail))
  }

  isAlive (width, height) {
    const collidesWithSelf = this.parts.filter(_ => this.collision(_, this.head)).length > 1
    const collidesWithEdge = this.head.x < 0 ||
                             this.head.x > width - 1 ||
                             this.head.y < 0 ||
                             this.head.y > height - 1
    return (!collidesWithEdge && !collidesWithSelf)
  }
  collision (a, b) {
    return (a.x === b.x && a.y === b.y)
  }
}
