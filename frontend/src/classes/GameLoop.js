/* global requestAnimationFrame, cancelAnimationFrame */
export default class GameLoop {
  constructor (renderFunction) {
    this.fps = 4
    this.speed = 1000 / this.fps
    this.renderFunction = renderFunction
    this.rafID = null
    this.loop = false
    this.startTime = 0
  }

  start () {
    console.log('Gameloop start')
    this.loop = true

    const renderFrame = (timestamp) => {
      if (this.loop) {
        if (timestamp - this.startTime > 1000 / this.fps) {
          this.renderFunction()
          this.startTime = timestamp
        }
      }
      this.fps *= 1.001
      this.rafID = requestAnimationFrame(renderFrame)
    }
    renderFrame()
  }

  pause () {
    console.log('Gameloop pause')
    this.loop = false
  }

  stop () {
    console.log('Gameloop stop')
    this.loop = false
    cancelAnimationFrame(this.rafID)
  }
}
